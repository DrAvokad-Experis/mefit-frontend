//----------Local Storage----------

//Add item to local storage
export const addLocalStorage = (key, value) => {
    localStorage.setItem(key, JSON.stringify(value));
}

//Read item from local storage
export const readLocalStorage = (key) => {
    const data = localStorage.getItem(key);
    if(data){
        return JSON.parse(data)
    }

    return null;
}

//Remove item from local storage
export const deleteLocalStorage = (key) => {
    localStorage.removeItem(key);
}

//----------Session Storage----------

//Add item to session storage
export const addSessionStorage = (key, value) => {
    sessionStorage.setItem(key, JSON.stringify(value));
}

//Read item from local storage
export const readSessionStorage = (key) => {
    const data = sessionStorage.getItem(key);
    if(data){
        return JSON.parse(data);
    }

    return null;
}

//Remove item from session storage
export const removeLocalStorage = (key) => {
    sessionStorage.removeItem(key);
}