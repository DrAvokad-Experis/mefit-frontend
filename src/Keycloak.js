import Keycloak from "keycloak-js";
const keycloak = new Keycloak({
  url: "https://mefitkc.azurewebsites.net/auth",
  realm: "MeFit",
  clientId: "MeFit-auth"
});

export default keycloak