import { useGoal } from "../../state/GoalContext";
import AddToUserComponent from "../AddToUserComponent";
import ProgramListItem from "./ProgramListItem";

const DashboardProgramListComponent = (props) => {
    const { programs } = props;
    const { goal, setGoal } = useGoal();

    //Creates a list of ProgramListItems and accompanying Add Program Buttons from the programs stored in the programs variable
    const programList = programs.map((program, index) => {
        return <div key={index}>
            <ProgramListItem program={program} key={program.id}></ProgramListItem>
            {(goal.program === 0 || goal.program === null) &&
                <AddToUserComponent trainingObject={program} objectType={"program"}></AddToUserComponent>
            }
        </div >
    })

    return (
        <div>
            <ul className="page-list">
                {programList}
            </ul>
        </div>
    )
}

export default DashboardProgramListComponent;