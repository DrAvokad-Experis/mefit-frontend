import { useWorkoutAPI } from "../../API/WorkoutAPI";
import { useState, useEffect } from "react";
import { useGoalAPI } from "../../API/GoalAPI";
import { addLocalStorage } from "../../utils/storage";
import { useUser } from "../../state/UserContext";
import { useGoal } from "../../state/GoalContext";
import { STORAGE_GOAL_KEY, STORAGE_USER_KEY, STORAGE_WORKOUT_KEY } from "../../const/storageKeys";
import { useWorkoutComplete } from "../../state/WorkoutCompleteContest";

const ProgramListItem = (props) => {
  //Program object
  const {
    apiAssignGoal,
    apiAssignProgramToGoal,
    apiCreateGoal,
    apiAssignWorkoutsToGoal,
  } = useGoalAPI();
  const { getWorkoutById } = useWorkoutAPI()
  const { setWorkoutComplete } = useWorkoutComplete()
  const { user, setUser } = useUser();
  const { program } = props;
  const [workouts, setWorkouts] = useState([]);
  const { goal, setGoal } = useGoal();

  useEffect(async () => {
    let newWorkouts = []
    for (let workoutId of program.workouts) {
      const workout = await getWorkoutById(workoutId);
        newWorkouts = [...newWorkouts, workout]
    }
    setWorkouts(newWorkouts);

  }, []);

  //Fetches and displays information about the workouts referenced in the program object
  const workoutsList = workouts.map((workout, index) => {
    return (
      <div key={index}>
        <p>{workout.name} - <i>{workout.type}</i></p>
        {workout.complete && <p>Workout Completed</p>}
      </div>
    );
  });

async function setGoalButton (program) {
    const newGoal = await apiCreateGoal();
    await apiAssignProgramToGoal(newGoal.id, program.id)
    await apiAssignWorkoutsToGoal(newGoal.id, program.workouts)
    newGoal.program = program.id
    newGoal.workouts = program.workouts
    await apiAssignGoal(user.id, newGoal.id);
    setGoal(newGoal);
    let newUser = user;
    newUser = {
      ...user,
      goal: newGoal.id,
    };
    setUser(newUser);
    setWorkoutComplete(null);
    addLocalStorage(STORAGE_WORKOUT_KEY, null);
    addLocalStorage(STORAGE_USER_KEY, newUser);
    addLocalStorage(STORAGE_GOAL_KEY, newGoal);
  };

  return (
    <div className="m-5 page-list-item">
      <h3>{program.name}</h3>
      <p>Category: {program.category}</p>
      <div>
        <p><b>Workouts:</b></p>
        {workoutsList}
      </div>
      <button className="btn" onClick={() => setGoalButton(program)}>Set Goal</button>
    </div>
  );
};

export default ProgramListItem;
