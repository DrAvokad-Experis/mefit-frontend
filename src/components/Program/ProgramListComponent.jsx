import { useState } from 'react'
import ProgramListItem from "./ProgramListItem";

const ProgramListComponent = (props) => {

    const { programs } = props;
    const [isListSorted, setIsListSorted] = useState(false);
   
const SortProgramsByCategory = () => {
    
    if (isListSorted){
        setIsListSorted(false)
    }
    else {
        setIsListSorted(true)
    }
}
    return (
        <div>
            <div>
            <strong>Sort programs by category</strong>
            <input type="checkbox" onChange={SortProgramsByCategory}/>
            </div>
            {!isListSorted &&
                <div className="page-list">
                {programs.sort((a,b) => a.name > b.name ? 1 : -1)
                 .map((program) => <ProgramListItem program={program} key={program.id}></ProgramListItem>)                    
                }
            </div>   
            }

            {isListSorted &&
             <div className="page-list">
                 {programs.sort((a,b) => a.category > b.category ? 1 : -1)
                 .map((program) => <ProgramListItem program={program} key={program.id}></ProgramListItem>)                    
                 }
             </div>   
                       
            }

        </div>


    )
}

export default ProgramListComponent;