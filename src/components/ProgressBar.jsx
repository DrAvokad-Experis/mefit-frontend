const ProgressBar = (props) => {

    //Color of the progress bar and percentage of the bar that should be filled represented by an int between 0 and 100.
    const { bgcolor, completed } = props;

    //CSS styling to make the progress bar look like a progress bar visually
    const containerStyles = {
        height: 20,
        width: '80%',
        backgroundColor: "#e0e0de",
        borderRadius: 50,
        margin: "0 auto"
      }

    const fillerStyles = {
        height: 20,
        width: `${completed}%`,
        backgroundColor: bgcolor,
        borderRadius: 'inherit',
        textAlign: 'right'
        
    }

    const labelStyles = {
        padding: 5,
        color: 'white',
        fontWeight: 'bold'
      }

    return (
        <div style={containerStyles}>
            <div style={fillerStyles}>
                <span style={labelStyles}>
                    {`${completed}%`}
                </span>
            </div>
        </div>
    )
}

export default ProgressBar;