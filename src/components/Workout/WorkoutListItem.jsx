import { useExerciseAPI } from '../../API/ExerciseAPI';
import { useState, useEffect } from 'react'

const WorkoutListItem = (props) => {
    const { getExerciseById } = useExerciseAPI()
    //Workout object
    const { workout } = props;

    //const [exerciseSet, setExerciseSet] = useState();
    const [exerciseList, setExerciseList] = useState([]);

    //Ignoring sets due to time restraint, replacing with just exercise
    //Fetches all exercises contained in a workout from the api
    useEffect(async () => {
        const newExerciseList = [];
        for(let exercise of workout.exercises){
            newExerciseList.push(await getExerciseById(exercise));
        }
        setExerciseList(newExerciseList);
    },[]);

    //Creates a list of all exercises contained in a workout
    const exerciseInfo = exerciseList.map((exercise, index) => {
        return (
            <div key={index}>
                <p>
                    {exercise.name}
                </p>
            </div>
        )
    })

    //Displays information about the sets of exercises referenced in the workout object.
    // const exercises = workout.set_id.map((set) => {
    //     return <p key={set}>{getExerciseById(getExerciseSet(set).exercise_id).name}, Repetitions {getExerciseSet(set).exercise_repetitions}</p>
    // })

    return (
        <div className="page-list-item">
            <h3>
                {workout.name}
            </h3>
            <p>
                Workout type: {workout.type}
            </p>
            <div>
                <p>
                    <b>
                        Exercises:
                    </b>
                </p>
                {exerciseInfo}
            </div>
            {/* <div>
                {exercises}
            </div> */}

            {/* {exerciseSet !== null && exerciseSet !== undefined &&
            <div>
                <p>
                Exercise: {exerciseSet.exercise.name}
                </p>
                <p>
                Repetitions: {exerciseSet.set.exerciseRepetitions}
                </p>
            </div>
            } */}
            
            {workout.complete &&
                <p>
                    Workout Completed
                </p>
            }
        </div>
    )
}

export default WorkoutListItem;