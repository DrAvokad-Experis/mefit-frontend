import { useState } from 'react'
import WorkoutListItem from "./WorkoutListItem";

const WorkoutListComponent = (props) => {

    const { workouts } = props;
    const [isListSorted, setIsListSorted] = useState(false);
    const SortWorkoutsByType = () => {
    
        if (isListSorted){
            setIsListSorted(false)
        }
        else {
            setIsListSorted(true)
        }
    }

    return (
        <div>
            <div className="text-center mt-10">
            <strong>Sort workouts by type</strong>
            <input type="checkbox" onChange={SortWorkoutsByType}/>
            </div>

            {!isListSorted &&
                <div className="page-list">
                    {
                        workouts.sort((a,b) => a.name > b.name ? 1 : -1)
                        .map((workout) => {
                            return <WorkoutListItem workout={workout} key={workout.id}></WorkoutListItem>
                        })
                    }                  
                </div>               
            }
            {isListSorted &&
                <div className="page-list">
                    {
                        workouts.sort((a,b) => a.type > b.type ? 1 : -1)
                        .map((workout) => {
                            return <WorkoutListItem workout={workout} key={workout.id}></WorkoutListItem>
                        })
                    }
                </div>              
            }         
        </div>
    )
}

export default WorkoutListComponent;