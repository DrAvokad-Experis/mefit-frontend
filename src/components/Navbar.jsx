import { NavLink } from "react-router-dom";
import { useKeycloak } from "@react-keycloak/web";
import { useEffect, useState } from "react";
import { useUser } from "../state/UserContext";
import { useProfileAPI } from "../API/ProfileAPI";
import { STORAGE_GOAL_KEY, STORAGE_USER_KEY } from "../const/storageKeys";
import { addLocalStorage, deleteLocalStorage } from "../utils/storage";
import { useGoalAPI } from "../API/GoalAPI";
import { useGoal } from "../state/GoalContext";

const Navbar = () => {
  const { apiGetGoal } = useGoalAPI();
  const { apiGetProfileLogin } = useProfileAPI();
  const { keycloak, initialized } = useKeycloak();
  const { user, setUser } = useUser();
  const { goal, setGoal } = useGoal();
  const [dropdown, setDropdown] = useState(false);

  //Extract the user info from the currently logged in keykloak user and uses it
  //to fetch the corresponding profile in the database or create a new if none exists.
  //The profile is then stored in state with contextAPI.
  const storeLogin = async () => {
    const userInfo = await keycloak.loadUserProfile();
    const userObject = {
      id: userInfo.id,
      username: userInfo.username,
      firstName: userInfo.firstName,
      lastName: userInfo.lastName,
    };
    const profile = await apiGetProfileLogin(userObject);
    setUser(profile);
    if (profile.goal !== (null || 0)) {
      const currentGoal = await apiGetGoal(profile.goal);
      setGoal(currentGoal);
      addLocalStorage(STORAGE_GOAL_KEY, currentGoal);
    }
    addLocalStorage(STORAGE_USER_KEY, profile);
  };

  // log out user with keycloak and delete local storage
  const handleLogout = async () => {
    deleteLocalStorage(STORAGE_USER_KEY);
    deleteLocalStorage(STORAGE_GOAL_KEY);
    keycloak.logout();
  };

  //Watches if a keycloak user logs in and if so, uses that user-info to fetch the correct profile.
  useEffect(() => {
    if (keycloak.authenticated) {
      storeLogin();
    }
  }, [keycloak.authenticated]);

  const hideDropdown = () => {
    setDropdown(false);
  };
  return (
    <nav className="nav">
      <NavLink className="nav-link" to="/">
        <div className="logo flex">
          <span className="material-icons logo-icon">fitness_center</span>
          <h1>MeFit</h1>
        </div>
      </NavLink>

      {keycloak.authenticated && (
        <>
          <ul className="flex nav-list">
            <NavLink className="nav-link" to="/">
              <li className="nav-item">Dashboard</li>
            </NavLink>
            <NavLink className="nav-link" to="/exercise">
              <li className="nav-item">Exercises</li>
            </NavLink>

            <NavLink className="nav-link" to="/workout">
              <li className="nav-item">Workouts</li>
            </NavLink>
            <NavLink className="nav-link" to="/program">
              <li className="nav-item">Programs</li>
            </NavLink>
            <NavLink className="nav-link" to="/goal">
              <li className="nav-item">Goals</li>
            </NavLink>
            {keycloak.tokenParsed.role.includes("Contributor") && 
            <NavLink className="nav-link" to="/contributor">
            <li className="nav-item">Contributor</li>
            </NavLink>
            }
            
          </ul>
          <div className="dropdown active">
            <li className="nav-item">
              <i
                onClick={() => setDropdown(!dropdown)}
                className="material-icons profile-icon"
              >
                account_circle
              </i>
            </li>

            <ul
              onMouseLeave={hideDropdown}
              className={dropdown ? "dropdown-list active" : "dropdown-list"}
            >
              <NavLink to="/profile">
                <li className="dropdown-item border-top">Profile page</li>
              </NavLink>
              <li onClick={handleLogout} className="dropdown-item border-bottom">
                Logout
              </li>
            </ul>
          </div>
        </>
      )}

      {!keycloak.authenticated && (
        <button onClick={() => keycloak.login()} className="nav-btn">
          Login
        </button>
      )}
    </nav>
  );
};

export default Navbar;
