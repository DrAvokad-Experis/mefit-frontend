const ExerciseListItem = (props) => {

    const { exercise } = props;

    return (
        <div className="page-list-item">
            <h3>
                <a href={exercise.videoLink}>
                    {exercise.name}
                </a>
            </h3>
            <h4>
                Target muscle group: {exercise.targetMuscleGroup}
            </h4>
            <p>
                {exercise.description}
            </p>
            <div className="exercise-item">
            <img src={exercise.img} className="exercise-image"></img>
            </div>
        </div>
    )
}

export default ExerciseListItem;