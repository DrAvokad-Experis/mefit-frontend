import ExerciseListItem from "./ExerciseListItem"

const ExerciseListComponent = (props) => {

    const { exercises } = props;

    //Creates a list of exercises from the exercise-objects stored in the exercises variable
    const exerciseList = exercises.map((exercise) => {
        return <ExerciseListItem exercise={exercise} key={exercise.id}></ExerciseListItem>
    })

    return (
        <div>
            <ul className="page-list">
                {exerciseList}
            </ul>
        </div>
    )
}

export default ExerciseListComponent