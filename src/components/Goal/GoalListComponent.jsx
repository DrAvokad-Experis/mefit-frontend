import DashboardProgramListComponent from "../Program/DashboardProgramListComponent";
import GoalExerciseListComponent from "./GoalExerciseListComponent";
import GoalWorkoutListComponent from "./GoalWorkoutListComponent";

const GoalListComponent = (props) => {

    const { programs, workouts, exercises } = props;

    return (
      <ul className="goal-list">
        <li className="goal-list-item">
          <h2>Programs</h2>
          <DashboardProgramListComponent programs={programs} />
        </li>
        <li className="goal-list-item">
          <h2>Workouts</h2>
          <GoalWorkoutListComponent workouts={workouts} />
        </li>
        <li className="goal-list-item">
          <h2>Exercises</h2>
          <GoalExerciseListComponent exercises={exercises} />
        </li>
      </ul>
    );
}

export default GoalListComponent;