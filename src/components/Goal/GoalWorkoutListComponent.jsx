import AddToUserComponent from "../AddToUserComponent";
import WorkoutListItem from "../Workout/WorkoutListItem";

const GoalWorkoutListComponent = (props) => {

    const { workouts } = props;

    //Creates a list of WorkoutListItems from the workout-objects stored in the workouts variable
    const workoutList = workouts.map((workout) => {
        return <div key={workout.id}>
            <WorkoutListItem workout={workout}></WorkoutListItem>
            <AddToUserComponent trainingObject={workout} objectType={"workout"}></AddToUserComponent>
        </div>
    })

    return (
        <div className="page-list">
            {workoutList}
        </div>
    )
}

export default GoalWorkoutListComponent;