import AddToUserComponent from "../AddToUserComponent";
import ExerciseListItem from "../Exercise/ExerciseListItem"

const GoalExerciseListComponent = (props) => {

    const { exercises } = props;

    //Creates a list of exercises with an accompanying AddToUserButton from the exercise-objects stored in the exercises variable
    const exerciseList = exercises.map((exercise) => {
        return <div key={exercise.id}>
            <ExerciseListItem exercise={exercise}></ExerciseListItem>
            <AddToUserComponent trainingObject={exercise}></AddToUserComponent>
        </div>
    })

    return (
        <div>
            <ul className="page-list">
                {exerciseList}
            </ul>
        </div>
    )
}

export default GoalExerciseListComponent