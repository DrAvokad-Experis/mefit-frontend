import { useGoalAPI } from "../API/GoalAPI";
import { useGoal } from "../state/GoalContext";
import { addLocalStorage } from "../utils/storage";

const AddToUserComponent = (props) => {
    const { apiAssignProgramToGoal, apiAssignWorkoutToGoal } = useGoalAPI()
    const { trainingObject, objectType } = props;
    const { goal, setGoal } = useGoal();

    //Adds trainingObject the button is bound to to the currently logged in user.
    const addTrainingObjectToUser = () => {
        let newGoal = goal;
        switch(objectType){
            case "program":
                apiAssignProgramToGoal(goal.id, trainingObject.id);
                newGoal = {
                    ...newGoal, program: trainingObject.id
                };
                setGoal(newGoal);
                addLocalStorage(newGoal);
                break;
            case "workout":
                apiAssignWorkoutToGoal(goal.id, trainingObject.id);
                let newWorkouts = newGoal.workouts
                newWorkouts = [...newWorkouts, trainingObject.id];
                newGoal = {
                    ...newGoal, workouts: newWorkouts
                };
                setGoal(newGoal);
                addLocalStorage(newGoal);
                break;
            default:
                console.log("Cannot add that yet")
        }
    }

    return (
        <div>
            <button className="add-btn" onClick={addTrainingObjectToUser}>Add to goal</button>
        </div>
    )
}

export default AddToUserComponent;  