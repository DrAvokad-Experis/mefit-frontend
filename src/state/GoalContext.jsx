import { STORAGE_GOAL_KEY } from '../const/storageKeys';
import { createContext, useContext, useState } from 'react';
import { readLocalStorage } from '../utils/storage';

const GoalContext = createContext();

export const useGoal = () => {
    return useContext(GoalContext)
};

const GoalProvider = (props) => {
    
    const [goal, setGoal] = useState(readLocalStorage(STORAGE_GOAL_KEY));
    const state = {
        goal,
        setGoal
    };

    return (
        <GoalContext.Provider value={state}>
            {props.children}
        </GoalContext.Provider>
    );
}

export default GoalProvider;