import GoalProvider from "./GoalContext";
import UserProvider from "./UserContext";
import WorkoutCompleteProvider from "./WorkoutCompleteContest";

const AppContext = (props) => {

    return (
        <UserProvider>
            <GoalProvider>
                <WorkoutCompleteProvider>
                    {props.children}
                </WorkoutCompleteProvider>
            </GoalProvider>
        </UserProvider>
    )
}

export default AppContext;