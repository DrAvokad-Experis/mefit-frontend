import { STORAGE_WORKOUT_KEY } from '../const/storageKeys';
import { createContext, useContext, useState } from 'react';
import { readLocalStorage } from '../utils/storage';

const WorkoutCompleteContext = createContext();

export const useWorkoutComplete = () => {
    return useContext(WorkoutCompleteContext)
};

const WorkoutCompleteProvider = (props) => {
    
    const [workoutComplete, setWorkoutComplete] = useState(readLocalStorage(STORAGE_WORKOUT_KEY));
    const state = {
        workoutComplete,
        setWorkoutComplete
    };

    return (
        <WorkoutCompleteContext.Provider value={state}>
            {props.children}
        </WorkoutCompleteContext.Provider>
    );
}

export default WorkoutCompleteProvider;