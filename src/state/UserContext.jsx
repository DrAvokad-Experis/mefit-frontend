import { STORAGE_USER_KEY } from '../const/storageKeys';
import { createContext, useContext, useState } from 'react';
import { readLocalStorage } from '../utils/storage';

const UserContext = createContext();

export const useUser = () => {
    return useContext(UserContext)
};

const UserProvider = (props) => {
    
    const [user, setUser] = useState(readLocalStorage(STORAGE_USER_KEY));
    const state = {
        user,
        setUser
    };

    return (
        <UserContext.Provider value={state}>
            {props.children}
        </UserContext.Provider>
    );
}

export default UserProvider;