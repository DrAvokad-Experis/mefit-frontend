import './App.css'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { ReactKeycloakProvider, useKeycloak } from '@react-keycloak/web'
import keycloak from './Keycloak'
import ExercisePage from "./views/ExercisePage.jsx"
import WorkoutPage from "./views/WorkoutPage.jsx"
import ProgramPage from "./views/ProgramPage.jsx"
import Navbar from "./components/Navbar.jsx"
import Dashboard from "./views/Dashboard.jsx"
import GoalPage from "./views/GoalPage.jsx"
import WelcomePage from './views/Homepage'
import PrivateRoute from './helpers/PrivateRoute'
import PublicRoute from './helpers/PublicRoute'
import ProfilePage from './views/ProfilePage'
import ContributerPage from './views/ContributerPage'

function App() {
  return (
    <>
      <ReactKeycloakProvider authClient={keycloak}>
        <BrowserRouter>
        <Navbar />

          <PublicRoute>
            <Routes>
              <Route
                path="/*"
                element={<WelcomePage />}
              />
            </Routes>
          </PublicRoute>

          <PrivateRoute>
            <Routes>              
              <Route path="/" element={<Dashboard />} />
              <Route path="/exercise" element={<ExercisePage />} />
              <Route path="/workout" element={<WorkoutPage />} />
              <Route path="/program" element={<ProgramPage />} />
              <Route path="/goal" element={<GoalPage />} />
              <Route path="/profile" element={<ProfilePage />} />
              <Route path="/contributor" element={<ContributerPage />} />
            </Routes> 
          </PrivateRoute>
        </BrowserRouter>
      </ReactKeycloakProvider>
    </>
  );

}

export default App;
