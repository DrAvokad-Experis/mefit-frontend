export const BASE_URL = "https://localhost:7272/api";
import { useKeycloak } from "@react-keycloak/web";

// Wrapper for fetching and resolving requests.
// Use the fetchWrapper custom hook with required request (get, post, put, delete).
// Bearer token is added for authorization.
// Supports parameter "overload" with a body which is parsed as json.
// -- Credit to Jason Watmore providing the code: https://jasonwatmore.com/post/2021/09/17/react-fetch-set-authorization-header-for-api-requests-if-user-logged-in -- 
export function useFetchWrapper() {
  const { keycloak, initialized } = useKeycloak();

  return {
    get: request("GET"),
    post: request("POST"),
    put: request("PUT"),
    delete: request("DELETE"),
  };

  function request(method) {
    return async (url, body) => {
      const requestOptions = {
        method,
        headers: authHeader(url),
      };
      if (body) {
        requestOptions.headers["Content-Type"] = "application/json";
        requestOptions.body = JSON.stringify(body);
      }
      const response = await fetch(url, requestOptions);
      return handleResponse(response);
    };
  }

  function authHeader(url) {
    // return auth header with jwt if user is logged in and request is to the api url
    const token = keycloak?.token;
    const isLoggedIn = !!token;
    const isApiUrl = url.startsWith(BASE_URL);
    if (isLoggedIn && isApiUrl) {
      return { Authorization: `Bearer ${token}` };
    } else {
      return {};
    }
  }

  function handleResponse(response) {
    return response.text().then((text) => {
      const data = text && JSON.parse(text);

      if (!response.ok) {
        if ([401, 403].includes(response.status) && keycloak?.token) {
          // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
          localStorage.removeItem("mefit-user");
          keycloak.logout();
        }

        const error = (data && data.message) || response.statusText;
        return Promise.reject(error);
      }

      return data;
    });
  }
}
