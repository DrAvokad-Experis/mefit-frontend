import { BASE_URL } from ".";
import { useFetchWrapper } from "./fetchWrapper";

export function useUserAPI() {
  const fetchWrapper = useFetchWrapper();

  return {
    apiGetUsers,
  };

  // Fetch all available users
  async function apiGetUsers() {
    const users = await fetchWrapper.get(`${BASE_URL}/users`);

    return users;
  }
}
