import { BASE_URL } from ".";
import { useFetchWrapper } from "./fetchWrapper";

export function useContributorAPI() {
  const fetchWrapper = useFetchWrapper();

  return {
    GetContributersProgramsWorkoutsExercisesById,
  };

  async function GetContributersProgramsWorkoutsExercisesById() {
    // if (ContributorId === 0) {
    //     return null;
    // }

    try {
      const contributions = await fetchWrapper.get(
        `${BASE_URL}/Profile/userContributions/1`
      );

      return contributions;
    } catch (error) {
      console.log(error);
    }
    return null;
  }
}
export const GetContributersProgramsWorkoutsExercisesById = async () => {
  // if (ContributorId === 0) {
  //     return null;
  // }

  try {
    const response = await fetch(`${BASE_URL}/Profile/userContributions/1`);
    const result = await response.json();

    return result;
  } catch (error) {
    console.log(error);
  }
  return null;
};
