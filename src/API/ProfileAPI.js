import { BASE_URL } from ".";
import { useFetchWrapper } from "./fetchWrapper";

export function useProfileAPI() {
  const fetchWrapper = useFetchWrapper();

  return {
    apiGetProfileById,
    apiGetProfileLogin,
    apiGetProfileByUserId,
    apiGetAddressById,
    apiGetProfileCategories,
    apiSetProfileCategories,
  };

  //Fetches a profile from the database with the corresponding profile id.
  async function apiGetProfileById(id) {
    try {
      const result = await fetchWrapper.get(`${BASE_URL}/Profile/${id}`);

      return result;
    } catch (error) {
      console.log(error);
    }
    return null;
  }

  //Handles user login. If a user already exists and is stored as a profile in the database it is fetched.
  //If a user logs in for the first time it is posted as a new profile in the database.
  async function apiGetProfileLogin(user) {
    try {
      const profile = await apiGetProfileByUserId(user.id);
      if (profile.userId === user.id) {
        console.log("User found");

        return profile;
      }
    } catch {
      try {
        const result = await fetchWrapper.post(
          `${BASE_URL}/profile?userId=${user.id}`,
          {
            userId: user.id,
            firstName: user.firstName,
            lastName: user.lastName,
            height: 0,
            weight: 0,
            categories: [],
          }
        );

        return result;
      } catch {
        console.log("Error occured during login!");
      }
      return null;
    }
  }

  //Fetches a profile from the database with the corresponding keycloak user id.
  async function apiGetProfileByUserId(id) {
    try {
      const profile = await fetchWrapper.get(`${BASE_URL}/profile/user/${id}`);

      if (profile.userId == id) {
        return profile;
      }

      return null;
    } catch (error) {
      console.log(error);
    }
    return null;
  }

  //Fetches an address object from the database if it exists. Otherwise returns null.
  async function apiGetAddressById(id) {
    try {
      const result = await fetchWrapper.get(`${BASE_URL}/Address/${id}`);

      return result;
    } catch (error) {
      console.log(error);
    }
    return null;
  }

  // Fetch categories from a specific profile
  async function apiGetProfileCategories(id) {
    try {
      const categories = await fetchWrapper.get(
        `${BASE_URL}/profile/categories/${id}`
      );

      return categories;
    } catch (error) {
      console.log(error);
    }
  }

  // Set a new list of categories for a profile
  async function apiSetProfileCategories(profileId, categoryIdList) {
    try {
      const result = await fetchWrapper.post(
        `${BASE_URL}/Profile/categories/${profileId}`,
        categoryIdList
      );

      return result;
    } catch (error) {
      console.log(error);
    }
    return null;
  }
}
