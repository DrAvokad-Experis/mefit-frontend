export const BASE_URL = "https://mefit-api.azurewebsites.net/api";

export const createHeaders = () => {
    return {
        'Content-Type': 'application/json'
    }
}