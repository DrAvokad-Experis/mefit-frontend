import { BASE_URL } from ".";
import { useFetchWrapper } from "./fetchWrapper";

export function useWorkoutAPI() {
  const fetchWrapper = useFetchWrapper();

  return {
    getWorkoutById,
    getWorkouts,
  };

  // Fetch a single workout by specifying the workout id
  async function getWorkoutById(workoutId) {
    try {
      const workout = await fetchWrapper.get(
        `${BASE_URL}/Workout/${workoutId}`
      );

      return workout;
    } catch (error) {
      console.log(error);
    }
    return null;
  }

  // Fetch all available workouts
  async function getWorkouts() {
    try {
      const workouts = await fetchWrapper.get(`${BASE_URL}/Workout`);

      return workouts;
    } catch (error) {
      console.log(error);
    }
    return null;
  }
}
