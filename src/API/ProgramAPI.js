import { BASE_URL } from ".";
import { useFetchWrapper } from "./fetchWrapper";

export function useProgramAPI() {
  const fetchWrapper = useFetchWrapper();

  return {
    getPrograms,
    getProgramById,
    apiGetProgramsByCategory,
    apiGetProgramsByCategories,
  };

  // Fetch all available programs
  async function getPrograms() {
    try {
      const programs = await fetchWrapper.get(`${BASE_URL}/GymProgram`);

      return programs;
    } catch (error) {
      console.log(error);
    }

    return null;
  }

  // Fetch a specific program by id
  async function getProgramById(programId) {
    try {
      const program = await fetchWrapper.get(
        `${BASE_URL}/GymProgram/${programId}`
      );

      return program;
    } catch (error) {
      console.log(error);
    }

    return null;
  }

  // Fetch every program in a specific category
  async function apiGetProgramsByCategory(category) {
    try {
      const programs = await fetchWrapper.get(
        `${BASE_URL}/gymprogram/category/${category}`
      );

      return programs;
    } catch (error) {
      console.log(error);
    }
  }

  // Fetch all programs in multiple categories
  async function apiGetProgramsByCategories() {
    try {
      const programs = await fetchWrapper.get(
        `${BASE_URL}/gymprogram/categories/`
      );

      return programs;
    } catch (error) {
      console.log(error);
    }
  }
}
