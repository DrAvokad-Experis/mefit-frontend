import { BASE_URL } from ".";
import { useFetchWrapper } from "./fetchWrapper";

export function useGoalAPI() {
  const fetchWrapper = useFetchWrapper();

  return {
    apiGetGoal,
    apiCreateGoal,
    apiAssignGoal,
    apiAssignProgramToGoal,
    apiAssignWorkoutToGoal,
    apiAssignWorkoutsToGoal,
    apiGetWorkoutsInGoal,
  };

  // Fetch a single goal by specifying the goal id
  async function apiGetGoal(id) {
    const data = await fetchWrapper.get(`${BASE_URL}/goal/${id}`);

    return data;
  }

  // Create a new goal with end date 7 days from now
  async function apiCreateGoal() {
    let startDate = new Date();
    let endDate = new Date();
    endDate.setDate(startDate.getDate() + 7);

    const newGoal = await fetchWrapper.post(`${BASE_URL}/goal`, {
      endDate: endDate,
      achieved: false,
    });

    const result = newGoal.value;

    return result;
  }

  // Assigns a new goal to a profile
  async function apiAssignGoal(profileId, goalId) {
    try {
      const response = await fetchWrapper.post(
        `${BASE_URL}/profile/goal/${profileId}`,
        goalId
      );

      console.log("assigned to profile", response);
    } catch (error) {
      console.log("error assigning goal: ", error);
    }
  }

  // Takes the id of a goal and the id of a program and assigns the corresponding program to the specified goal.
  async function apiAssignProgramToGoal(goalId, programId) {
    try {
      const response = await fetchWrapper.post(
        `${BASE_URL}/goal/program/${goalId}`,
        programId
      );

      console.log("program assigned to goal: ", response);
    } catch (error) {
      console.log(error);
    }
  }

  // Takes the id of a goal and the id of a workout and assigns the corresponding workout to the specified goal.
  async function apiAssignWorkoutToGoal(goalId, workoutId) {
    try {
      const response = await fetchWrapper.post(
        `${BASE_URL}/goal/workout/${goalId}`,
        workoutId
      );

      console.log("workout assigned to goal: ", response);
    } catch (error) {
      console.log(error);
    }
  }

  // Assign a list of workouts to a goal specified by a goal id
  async function apiAssignWorkoutsToGoal(goalId, workoutIdList) {
    try {
      const result = await fetchWrapper.post(
        `${BASE_URL}/Goal/workouts/${goalId}`,
        workoutIdList
      );

      return result;
    } catch (error) {
      console.log(error);
    }
  }

  // Fetch all workouts in the specified goal
  async function apiGetWorkoutsInGoal(goalId) {
    try {
      const result = await fetchWrapper.get(
        `${BASE_URL}/Goal/workouts/${goalId}`
      );
      console.log("Workouts gotten from goal: ", result)
      return result;
    } catch (error) {
      console.log(error);
    }
  }
}
