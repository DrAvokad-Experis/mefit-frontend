const exercisesH = [
    { 
        "id":1,
        "name":"Zercher Shrugs",
        "description":"Shrug your shoulders while holding a barbell with your elbows",
        "target_muscle_group":"trapezious",
        "image":zercherShrug,
        "vid_link":"https://www.youtube.com/watch?v=1LsIQr_4iSY&t=22s&ab_channel=KYRIAKOSKAPAKOULAK"
    },
    {
        "id":2,
        "name":"Log Press",
        "description":"Lift the log from the ground to above your head",
        "target_muscle_group":"deltoid",
        "image":logPress,
        "vid_link":"https://www.youtube.com/watch?v=6zJsptf0T8c&ab_channel=StrongmanTalk"
    },
    {
        "id":3,
        "name":"Guitar Lunge",
        "description":"Lunge forward with one leg until your knee is at a 90 degree angle whilst carrying a bar on your shoulders and simultaneusly playing the guitar",
        "target_muscle_group":"the mindset",
        "image":guitarLunge,
        "vid_link":"https://www.youtube.com/watch?v=lSmlAAdwC1o&ab_channel=EricBugenhagen"
    },
    {
        "id":4,
        "name":"Snatch",
        "description": "Lift a barbell from the ground to overhead in one continuous motion",
        "target_muscle_group":"full body",
        "image":snatch,
        "vid_link":"https://www.youtube.com/watch?v=SW1IHQzua6k&ab_channel=Clarence0"
    }

];

const workoutsH = [
    {
        "id":1,
        "name":"For the Difficult",
        "type":"Unorthodox Lifts",
        "complete":false,
        "set_id":[
            1,
            3
        ]
    },
    {
        "id":2,
        "name":"Overhead Master",
        "type":"Full Body",
        "complete":true,
        "set_id":[
            2,
            4,
        ]
    }
]