import { BASE_URL } from ".";
import { useFetchWrapper } from "./fetchWrapper";

export function useCategoryAPI() {
  const fetchWrapper = useFetchWrapper();
  return {
    apiGetCategories,
  };

  // Fetch all categories
  async function apiGetCategories() {
    const categories = await fetchWrapper.get(`${BASE_URL}/category`);

    return categories;
  }
}
