import { BASE_URL } from ".";
import { useFetchWrapper } from "./fetchWrapper";

export function useExerciseAPI() {
  const fetchWrapper = useFetchWrapper();

  return {
    getExerciseSetObjectById,
    getExerciseSetById,
    getExerciseById,
    getExercises,
  };

  async function getExerciseSetObjectById(setId) {
    if (setId === 0) {
      return null;
    }

    try {
      const set = await fetchWrapper.get(`${BASE_URL}/Set/${setId}`);
      const exercise = await fetchWrapper.get(
        `${BASE_URL}/Exercise/${set.exercise}`
      );
      const result = {
        set: set,
        exercise: exercise,
      };

      return result;
    } catch (error) {
      console.log(error);
    }
    return null;
  }

  //Takes a set-id as an argument and returns the exercise-set with the corresponding id if it exists in the database.
  async function getExerciseSetById(setId) {
    if (setId === 0) {
      return null;
    }
    try {
      const set = await fetchWrapper.get(`${BASE_URL}/Set/${setId}`);

      return set;
    } catch (error) {
      console.log(error);
    }

    return null;
  }

  //Takes an exercise-id as an argument and returns the exercise with the corresponding id if it exists in the database.
  async function getExerciseById(exerciseId) {
    try {
      const exercise = await fetchWrapper.get(
        `${BASE_URL}/Exercise/${exerciseId}`
      );

      return exercise;
    } catch (error) {
      console.log(error);
    }

    return null;
  }

  //Returns all exercises stored in the database.
  async function getExercises() {
    try {
      const exercises = await fetchWrapper.get(`${BASE_URL}/Exercise`);

      return exercises;
    } catch {
      console.log(error);
    }

    return null;
  }
}
