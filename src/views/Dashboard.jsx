import ProgressBar from "../components/ProgressBar";
import { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { useProgramAPI } from "../API/ProgramAPI";
import { useUser } from "../state/UserContext";
import { useGoal } from "../state/GoalContext";
import { STORAGE_GOAL_KEY, STORAGE_WORKOUT_KEY } from "../const/storageKeys";
import { addLocalStorage, removeLocalStorage } from "../utils/storage";
import { useKeycloak } from "@react-keycloak/web";
import ProgramListComponent from "../components/Program/ProgramListComponent";
import { useGoalAPI } from "../API/GoalAPI";
import { useProfileAPI } from "../API/ProfileAPI";
import { useWorkoutComplete } from "../state/WorkoutCompleteContest";

const Dashboard = () => {
  const { user, setUser } = useUser();
  const { goal, setGoal } = useGoal();
  const { keycloak, initialized } = useKeycloak();
  const { workoutComplete, setWorkoutComplete } = useWorkoutComplete();
  const { apiGetWorkoutsInGoal } = useGoalAPI();
  const { apiGetProfileCategories } = useProfileAPI();
  const { apiGetProgramsByCategory, getProgramById } = useProgramAPI();

  const [programs, setPrograms] = useState([]);
  const [categories, setCategories] = useState([]);
  const [program, setProgram] = useState();
  const [workouts, setWorkouts] = useState();
  const [progress, setProgress] = useState(0);

  // Fetch data from api when workoout is complete or when user or goal is changed
  useEffect(() => {
    const fetchCategories = async () => {
      setCategories(await apiGetProfileCategories(user.id));
    };
    const fetchProgram = async () => {
      setProgram(await getProgramById(goal.program));
    };
    const fetchWorkouts = async () => {
      setWorkouts(await apiGetWorkoutsInGoal(goal.id));
    };
    if (goal != null) {
      if (goal.program != 0) fetchProgram().catch(console.error);
      if (goal.workouts.length != 0) fetchWorkouts().catch(console.error);
    }
    if (user != null) fetchCategories().catch(console.error);
    calcProgress();
  }, [goal, user, workoutComplete]);

  // Fetch all programs based on user categories
  const handleLoadPrograms = async () => {
    let programsResult = [];
    if (programs.length == 0) {
      for (let category of categories) {
        console.log(category.name);
        const programsData = await apiGetProgramsByCategory(category.name);
        console.log(programsData);
        programsResult.push(...programsData);
      }
      setPrograms(programsResult);
    }
  };

  // Capitalize username
  const username =
    keycloak.tokenParsed.preferred_username[0].toUpperCase() +
    keycloak.tokenParsed.preferred_username.slice(
      1,
      keycloak.tokenParsed.preferred_username.length
    );

  //Hardcoded valus for progress bar. Replace when user is added.
  const progressBar = { bgcolor: "#641b9b", completed: 40 };

  // Remove goal data 
  const removegoal = () => {
    setGoal(null);
    setWorkouts(null);
    setProgram(null);
    // setProgress(0)
    setWorkoutComplete(null);
    addLocalStorage(STORAGE_WORKOUT_KEY, null);
    removeLocalStorage(STORAGE_GOAL_KEY, null);
  };

  // Finds the next workout that has not been completed
  const findCurrentWorkout = () => {
    return workouts.find((w) => w.complete == false);
  };

  // Component for the next workout
  function NextWorkout(props) {
    const workoutList = props.workoutList;
    if (workoutList != undefined) {
      const nextWorkout = findCurrentWorkout();
      if(nextWorkout !== undefined){
        return <h3>Next Workout: {nextWorkout.name}</h3>;
      }
    }
    return <></>;
  }

  // Finds the current workout and set it to complete
  const handleCompleteWorkout = () => {
    const index = workouts.indexOf(findCurrentWorkout());
    workouts[index].complete = true;
    setCompleteWorkout(workouts[index]);
  };

  // handles the completion of workouts in the local storage
  const setCompleteWorkout = (workout) => {
    let newWorkoutList = [workout];
    newWorkoutList =
      workoutComplete !== null
        ? newWorkoutList.concat(workoutComplete.workoutComplete)
        : newWorkoutList;
    const newWorkoutObject = {
      workoutComplete: newWorkoutList,
    };
    setWorkoutComplete(newWorkoutObject);
    addLocalStorage(STORAGE_WORKOUT_KEY, newWorkoutObject);
  };

  // Calculates the progress based on complete workouts
  const calcProgress = () => {
    if (workoutComplete === null) {
      setProgress(0);
    } else if (workoutComplete.length !== 0) {
      setProgress(
        parseInt(
          100 * (workoutComplete.workoutComplete.length / goal.workouts.length)
        )
      );
    } else {
      setProgress(0);
    }
  };

  return (
    <div>
      <h3 className="text-center my-10 underline">{username} Dashboard</h3>
      {goal != null && (
        /* Renders when user has a goal */
        <div className="text-center h-100">
          {progress < 100 ? (
            <div>
              <h2>Current Program: {program != undefined && program.name}</h2>
              <NextWorkout
                workoutList={workouts != undefined ? workouts : null}
              />
              <button className="btn my-4" onClick={handleCompleteWorkout}>
                Complete workout
              </button>
            </div>
          ) : (
            <div>
              <h3>🎉 Goal Completed! 🎉</h3>
              <button className="btn my-4" onClick={removegoal}>
                Set new goal
              </button>
            </div>
          )}
          <ProgressBar
            bgcolor={progressBar.bgcolor}
            completed={progress}
          ></ProgressBar>
          <p>Goal expires at: {goal.endDate.slice(0, 10)}</p>
          <h4 className="mt-10">
            <NavLink to="/goal">
              Want to explore more programs and exercise to set for your goal?
              Click here!
            </NavLink>
          </h4>
        </div>
      )}
      <div>
        {goal == null && (
          <div className="text-center">
            {categories.length != 0 ? (
              <div>
                {programs.length == 0 ? (
                  /* Renders when user has no goal, no categories set 
                      and no programs has been loaded*/
                  <div className="text-center mt-10">
                    <h4>Check out your recommended programs below:</h4>
                    <button
                      className="mt-4 btn w-50"
                      onClick={handleLoadPrograms}
                    >
                      Load Programs
                    </button>
                  </div>
                ) : (
                  /* Renders when user has no goal, but categories are set
                         and load programs button has been pressed */
                  <div>
                    <h3 className="my-8 text-center">
                      Recommended programs just for you 🥰
                    </h3>
                    <ProgramListComponent
                      programs={programs != null ? programs : null}
                    ></ProgramListComponent>
                  </div>
                )}
              </div>
            ) : (
              /* Renders when user has no goal and no categories set */
              <div>
                <h4>You currently have no active goal.</h4>
                <h4 className="text-center mt-10">
                  Set your prefered program categories in your profile to get
                  recommended programs.
                </h4>
              </div>
            )}
          </div>
        )}
      </div>
    </div>
  );
};

export default Dashboard;
