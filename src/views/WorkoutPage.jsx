import WorkoutListComponent from "../components/Workout/WorkoutListComponent";
import { useEffect, useState } from "react";
import { useWorkoutAPI } from "../API/WorkoutAPI";

const WorkoutPage = () => {
  const { getWorkouts } = useWorkoutAPI();
  const [workouts, setWorkouts] = useState([]);

  //Load existing workouts in database from API.
  useEffect(async () => {
    setWorkouts(await getWorkouts());
  }, []);

  return (
    <div className="mt-10">
      <h1 className="mx-auto border-b-2 border-black border-solid w-[8.5rem]">
        Workouts
      </h1>
      <WorkoutListComponent workouts={workouts} />
    </div>
  );
};

export default WorkoutPage;
