import { useKeycloak } from "@react-keycloak/web";
import { useState, useEffect } from "react";
import { useCategoryAPI } from "../API/CategoryAPI";
import { useProfileAPI } from "../API/ProfileAPI";
import { useUser } from "../state/UserContext";
export const STORAGE_USER_KEY = "mefit-user";
import { addLocalStorage } from "../utils/storage";
import { capitalizeFirstLetter } from "../helpers/StringHelpers";

const ProfilePage = () => {
  const { user, setUser } = useUser();
  const { initialized, keycloak } = useKeycloak();
  const { apiGetCategories } = useCategoryAPI();
  const { apiGetAddressById, apiSetProfileCategories } = useProfileAPI();

  const [profile, setProfile] = useState(null);
  const [address, setAddress] = useState({
    address: "",
  });
  const [roles, setRoles] = useState([]);
  const [categories, setCategories] = useState([]);

  useEffect(async () => {
    setRoles(...roles, keycloak.idTokenParsed.role);
    setProfile(user);
    setCategories(await apiGetCategories());

    const apiAddress = await apiGetAddressById(user.id);
    if (apiAddress != null) {
      setAddress(apiAddress);
    }
  }, []);
  //Updates the user-object of the currently logged in user with the value input in the form.
  //Used to update profile related information
  const handleInputChangeProfile = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    let newProfile = profile;
    newProfile = {
      ...newProfile,
      [name]: value,
    };
    setProfile(newProfile);
  };

  //Updates the user-object of the currently logged in user with the value input in the form.
  //Used to update address related information
  const handleInputChangeAddress = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    // let newAddress = address.address;
    // newAddress = {
    //     ...newAddress, [name]:value
    // }
    let newAddress = value;
    setAddress(newAddress);
  };

  // Return a list of category input checkboxes from the fetched categories
  const categoryList = categories.map((category) => {
    return (
      <div className="category-item" key={category.id}>
        <input
          type="checkbox"
          className="category-box"
          id={category.id}
          name={category.name}
        ></input>
        <label className="category-label" htmlFor={category.name}>
          {category.name}
        </label>
      </div>
    );
  });

  // fetch categories and save to profile
  const handleFetchCategories = () => {
    const categoryElements = document.getElementsByClassName("category-box");
    const categoryIdList = [];
    const categoryNameList = [];
    for (let category of categoryElements) {
      if (category.checked) {
        categoryIdList.push(category.id);
        categoryNameList.push(category.name);
      }
    }

    apiSetProfileCategories(profile.id, categoryIdList);
    let newProfile = profile;
    newProfile = {
      ...newProfile,
      categories: categoryNameList,
    };
    setProfile(newProfile);
    setUser(newProfile);
    addLocalStorage(STORAGE_USER_KEY, newProfile);
  };

  // Save data on submit click
  const handleSubmit = (event) => {
    event.preventDefault();
    handleFetchCategories();
  };

  // Function to set contributor role
  const requestContributor = () => {
    // Send imagninary email to admin
  };
  const logToken = () => {
    console.log(keycloak.idTokenParsed.role);
    //console.log(keycloak.token)
  };

  //Implement submit button and function when redux is implemented
  return (
    <div className="mt-6 ml-6 profile-container">
      <h3 className="mb-4">
        {capitalizeFirstLetter(keycloak.idTokenParsed.preferred_username)} (
        {roles[roles.length - 1]})
      </h3>
      {profile !== null && (
        <form onSubmit={handleSubmit} className="profile-form flex flex-col">
          <label className="input-label" htmlFor="firstName">
            First Name:
          </label>
          <input
            name="firstName"
            type="text"
            value={profile.firstName}
            onChange={handleInputChangeProfile}
          />
          <label className="input-label" htmlFor="lastName">
            Last Name:
          </label>
          <input
            name="lastName"
            type="text"
            value={profile.lastName}
            onChange={handleInputChangeProfile}
          />
          <label className="input-label" htmlFor="addressLine">
            Address:
          </label>
          <input
            name="addressLine"
            type="text"
            value={address.address}
            onChange={handleInputChangeAddress}
          />
          <label className="input-label" htmlFor="weight">
            Weight:
          </label>
          <input
            name="weight"
            type="number"
            value={profile.weight}
            onChange={handleInputChangeProfile}
          />
          <label className="input-label" htmlFor="height">
            Height:
          </label>
          <input
            name="height"
            type="number"
            value={profile.height}
            onChange={handleInputChangeProfile}
          />
          <div className="mt-2">
            <p>
              <b>Goals with your training: </b>
            </p>
            {categories != null ? categoryList : null}
          </div>
          <button className="submit-btn" type="submit" value="Submit">
            Submit
          </button>
          <button className="btn w-32" onClick={logToken}>
            Log token
          </button>
        </form>
      )}
      {!roles.includes("Contributor") && (
        <button onClick={requestContributor} className="btn mt-2">
          Request for contributor status
        </button>
      )}
    </div>
  );
};

export default ProfilePage;
