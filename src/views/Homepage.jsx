import React from 'react';
import { useKeycloak } from '@react-keycloak/web';


const Home = () => {
  const { keycloak, initialized} = useKeycloak();

 return (
   <div className="text-center mt-10">
   <h1>Welcome to Me Fit</h1>
   <p>Please <span className="text-blue-600 underline cursor-pointer" onClick={() => keycloak.login()}>log in</span> to start using the application</p>
   </div>
 );
};

export default Home;