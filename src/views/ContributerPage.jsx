import { useEffect, useState } from 'react';
import { GetContributersProgramsWorkoutsExercisesById } from '../API/ContributerAPI'

const ContributerPage = () => {

    const [contributor, setContributor] = useState([])
    const [exersciseList, setExercises] = useState([])
    const [programList, setPrograms] = useState([])
    const [workoutList, setWorkouts] = useState([])
    //Load existing exercises in database from API on mounting page.
    useEffect(async () => {
        setContributor(await GetContributersProgramsWorkoutsExercisesById());
    }, [])

    //Creates a list of exercises from the exercise-objects stored in the exercises variable
    // const exerciseList = contributor.exercises.map((exercise) => {
    //     return <ExerciseListItem exercise={exercise} key={exercise.id}></ExerciseListItem>
    // })
    
    const LoadProfileExercisesProgramsWorkouts = () => {
        
        const exerciseList = contributor.exercises.map(exercise => {
            return <div key={exercise.id}><span>{exercise.name}</span><br /></div>
            })
            setExercises(exerciseList)
            
        const programList = contributor.gymPrograms.map(gymProgram => {
            return <div key={gymProgram.id}><span>{gymProgram.name}</span><br /></div>
             })
             setPrograms(programList)
        const workoutList = contributor.workouts.map(workout => {
            return <div key={workout.id}><span>{workout.name}</span><br /></div>
             })
             setWorkouts(workoutList)
    }

    return (
        <div>
            <h1>Contributor Page</h1>
            <h1>{contributor.firstName}</h1>
            <button onClick={LoadProfileExercisesProgramsWorkouts}>Hämta</button>
            <div>
                <h1>Exercises</h1>
                {exersciseList}
            </div>
            <div>
                <h1>Programs</h1>
                {programList}
            </div>
            <div>
                <h1>Workouts</h1>
                {workoutList}
            </div>
        </div>
    )
}

export default ContributerPage