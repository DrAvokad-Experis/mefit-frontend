import ProgramListComponent from "../components/Program/ProgramListComponent";
import { useState, useEffect } from "react";
import { useProgramAPI } from "../API/ProgramAPI";

const ProgramPage = () => {
  const { getPrograms } = useProgramAPI();
  const [programs, setPrograms] = useState([]);

  //Load existing programs in database from API.
  useEffect(async () => {
    setPrograms(await getPrograms());
  }, []);

  return (
    <div className="mt-10">
      <h1 className="mx-auto border-b-2 border-black border-solid w-[8.5rem]">
        Programs
      </h1>

      <ProgramListComponent programs={programs} />
    </div>
  );
};

export default ProgramPage;
