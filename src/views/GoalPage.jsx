import GoalListComponent from "../components/Goal/GoalListComponent";
import { useGoalAPI } from "../API/GoalAPI";
import { useProgramAPI } from "../API/ProgramAPI";
import { useWorkoutAPI } from "../API/WorkoutAPI";
import { useExerciseAPI } from "../API/ExerciseAPI";
import ProgressBar from "../components/ProgressBar";
import { useState, useEffect } from "react";
import { useGoal } from "../state/GoalContext";
import { useUser } from "../state/UserContext";
import { useWorkoutComplete } from "../state/WorkoutCompleteContest";

const GoalPage = () => {
  const { apiCreateGoal, apiAssignGoal } = useGoalAPI();
  const { getExercises } = useExerciseAPI();
  const { getPrograms } = useProgramAPI();
  const { getWorkouts } = useWorkoutAPI();

  const { goal, setGoal } = useGoal();
  const { user, setUser } = useUser();
  const { workoutComplete, setWorkoutComplete } = useWorkoutComplete();
  const [programs, setPrograms] = useState([]);
  const [workouts, setWorkouts] = useState([]);
  const [exercises, setExercises] = useState([]);
  const [progress, setProgress] = useState(0);

  useEffect(() => {
    calcProgress();
  }, [workoutComplete]);

  useEffect(async () => {
    setPrograms(await getPrograms());
    setWorkouts(await getWorkouts());
    setExercises(await getExercises());
  }, []);
  const setGoalButton = async () => {
    const newGoal = await apiCreateGoal();
    await apiAssignGoal(user.id, newGoal.id);
    setGoal(newGoal);
    let newUser = user;
    newUser = {
      ...user,
      goal: newGoal.id,
    };
    setUser(newUser);
    addLocalStorage(STORAGE_USER_KEY, newUser);
    addLocalStorage(STORAGE_GOAL_KEY, newGoal);
  };

  const logGoal = () => {
    console.log(goal);
    console.log("progress: ", progress);
  };

  const calcProgress = () => {
    if (workoutComplete === null) {
      setProgress(0);
    } else if (workoutComplete.length !== 0) {
      setProgress(
        parseInt(
          100 * (workoutComplete.workoutComplete.length / goal.workouts.length)
        )
      );
    } else {
      setProgress(0);
    }
  };

  return (
    <div>
      {goal === null && <button onClick={setGoalButton}>Set New Goal</button>}
      {goal !== null && (
        <div className="mt-20">
          <p className="flex justify-center">Goal deadline: {goal.endDate}</p>
          <ProgressBar bgcolor="#641b9b" completed={progress}></ProgressBar>
          {
            <GoalListComponent
              programs={programs}
              workouts={workouts}
              exercises={exercises}
            />
          }
        </div>
      )}
    </div>
  );
};

export default GoalPage;
