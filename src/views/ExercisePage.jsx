import ExerciseListComponent from '../components/Exercise/ExerciseListComponent';
import { useEffect, useState } from 'react';
import { useExerciseAPI } from '../API/ExerciseAPI';

const ExercisePage = () => {
    const { getExercises } = useExerciseAPI()
    const [exercises, setExercises] = useState([]);

    //Load existing exercises in database from API on mounting page.
    useEffect(async () => {
        setExercises(await getExercises());
    }, [])

    return (
        <div className="mt-10">
            <h1 className="mx-auto border-b-2 border-black border-solid w-32">Exercises</h1>
            <ExerciseListComponent exercises={exercises}/>
        </div>
    )
}

export default ExercisePage;