import { useKeycloak } from "@react-keycloak/web"

const PublicRoute = ({ children }) => {
  const { keycloak } = useKeycloak()

  const isLoggedIn = keycloak.authenticated

  return isLoggedIn ? null : children
}

export default PublicRoute