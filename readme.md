# MeFit - Frontend


[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![web](https://img.shields.io/static/v1?logo=icloud&message=Online&label=web&color=success)](https://mefit-noroff-swe.herokuapp.com/)

Fitness Tracker Web Application Written in C# & React

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background

This is a case project about creating a web application with a RESTful API provided by noroff.


## Install

To install and use this frontend in development you will need an instance of keycloak running on docker as an image, an instance of the backend running locally and node and npm to run the frontend locally in development-mode.

To set-up a correct keycloak image make sure that you have docker installed then open a terminal and run:

```sh
docker pull jboss/keycloak
```

This will download a jboss/keycloak image. To set it up run:

```sh
docker run --name mefit-keycloak -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin -p 8080:8080 jboss/keycloak
```

When the image is running in a docker container go to localhost:8080 and open the keycloak admin console. Login with username admin and password admin. Create a new realm called MeFit. When created, create a client called MeFit-auth. In the client fill the following values:
- Valid Redirect URIs: http://localhost:3000/*
- Web Origins: http://localhost:3000/*

Save the updates to the client.

In `Realm settings` / `Login` enable user registration and save.

Finally edit the url in the Keycloak.js file in the repo:

```sh
url: "https://localhost:8080/auth"
```

## Usage

Either use the deployed web application linked at the top or,

Open a terminal or powershell window and run:

```sh
npm run dev
```

## Maintainers

[Aldin Drobic (@ADrobic)](https://gitlab.com/Adrobic)
[Alexander Grönberg (@DrAvokad)](https://gitlab.com/DrAvokad)
[Rasmus Möller (@achieuw)](http://gitlab.com/achieuw)

## Acknowledgements



## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 Aldin Drobic, Alexander Grönberg & Rasmus Möller
